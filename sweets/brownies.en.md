# brownies

  - time work: 10min
  - time wait: 30min

## ingredients

  - 500g butter
  - 200g cocoa powder
  - 8x eggs
  - 1x vanille aroma
  - 600g sugar
  - 250g flour
  - 200g chopped almonds
  - icing sugar

## preparation

  1. meld the butter in the pot
  2. take it from the stove when completly fluid
  3. put all ingredients except the icing sugar in it
  4. mix it into a dough
  5. take a baking plate, put on baking paper and spread the dough on it
  6. put it in the oven at around 175-200°C and bake it for 25-30min
  7. when ready put the icing sugar on it, let it cool down and cut it into
     peaces of your liking

