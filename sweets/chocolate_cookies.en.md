# chocolate cookies (vegan/gluten-free)

  - time work: 25min
  - time wait: 12min

## ingredients

1 cup = 250ml/250mg

  - 1x cup butter (vegan: 1x cup margarine)
  - 1x cup white sugar
  - 1x cup brown sugar
  - 3x eggs (vegan: 3x ripe mashed bananas)
  - 1x cup peanut butter
  - 1x cup oat flakes (leave for gluten-free)
  - 1x cup shredded coconut
  - 1x cup raisins or raspberries
  - 1x cup shredded chocolate (vegan: dark chocolate) [TIP: Use Ritter sport dark and/or marzipan]
  - 1x cup flour (optionaly use gluten-free)
  - 1x cup shredded nuts
  - 1x teaspoon baking soda
  - 1x teaspoon baking powder
  - 1x teaspoon salt
  - 1x teaspoon vanilla sugar

## preparation

  0. meld butter/margarine
  1. stir butter/margarine and peanut butter until it's creamy
  2. add all the other ingredients one at a time, eggs/bananas last
  4. mix it into a dough
  5. take a baking plate, put on baking paper and put small heaps of the dough
     on it
  6. put it in the oven at around 180°C and bake it for 10-12min (don't let the raisins/raspberries get black)

`3.5 x 15min`
