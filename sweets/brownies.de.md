# brownies

  - arbeitszeit: 10min
  - backzeit: 30min

## zutaten

  - 500g butter
  - 200g kakaopulver
  - 8x eier
  - 1x vanille aroma
  - 600g zucker
  - 250g mehl
  - 200g mandeln gehackt
  - puderzucker

## zubereitung

  1. butter im topf zerlassen
  2. butter vom herd nehmen
  3. alle zutaten (bis auf den puderzucker) in die butter geben
  4. zu einem teig verarbeiten
  5. backblech mit backpapier auslegen und die masse darauf verteilen
  6. bei ca. 175-200°C 25-30 min backen
  7. teig mit puderzucker bestäuben, auskühlen lassen und in quadrate schneiden
