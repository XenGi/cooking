[Plinse](https://de.wikipedia.org/wiki/Plinse)
==============================================

Ingredients
-----------

  - 2 Eggs
  - Flour
  - Milk
  - (Mineral water)
  - (Sugar)
  - Pinch of salt

Steps
-----

1. Mix everything together
2. Consistency should be fluid but starts sticking to your finger
3. Put the dough in a hot pan
4. Turn after roughly a minute
